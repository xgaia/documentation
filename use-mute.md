# Muting other accounts/instances

## How to mute an instance/account

As a user, you can hide other accounts content.
To mute an account, go on the account page (for example: https://peertube3.cpy.re/accounts/chocobozzz@peertube2.cpy.re) and click on <i data-feather="more-horizontal"></i>.

Then, you can decide to:
 * Hide the account content
 * Hide the remote account's instance content

User mutes are listed in `My account > Misc > Muted accounts` and `My account > Misc > Muted instances`.

If you are an administrator or a moderator, you can also:
 * Hide the account content for all the users of your instance
 * Hide the remote account's instance content for all the users of your instance

Instance mutes are listed in `Administration > Moderation > Muted accounts` and `Administration > Moderation > Muted servers`.

 ![](/assets/mute-account.png)

## What does mute do?

When you mute an account, all its content will be hidden:
 * Its videos won't be listed anymore
 * Its comments won't be displayed anymore
 * New comments by this account/instance on your videos won't be forwarded to other instances
